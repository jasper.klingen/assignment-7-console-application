﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment7console.Models
{
    public class Character
    {
        [Key]
        public int CharacterId { get; set; }

        [Required]
        [MaxLength(50)]
        public string FullName { get; set; }

        [MaxLength(50)]
        public string Alias { get; set; }

        [MaxLength(50)]
        public string Gender { get; set; }

        [MaxLength(100)]
        public string PictureURL { get; set; }

        //Defines relationship between characters and movies, one character can have many movies, hence the ICollection of movies, and one movie can have many characters
        public ICollection<Movie> Movies { get; set; }
    }
}
