﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment7console.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class Franchise
    {
        [Key]
        public int FranchiseId { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [MaxLength(200)]
        public string Description { get; set; }
        
        //Defines relationship between movies and franchises, one franchise can have many movies, hence the ICollection of movies
        public ICollection<Movie> Movies { get; set; }
    }
}
