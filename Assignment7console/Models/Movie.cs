﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment7console.Models
{
    public class Movie
    {
        [Key]
        public int MovieId { get; set; }

        [Required]
        [MaxLength(50)]
        public string MovieTitle { get; set; }

        [MaxLength(50)]
        public string Genre { get; set; }

        public int Year { get; set; }

        [MaxLength(50)]
        public string Director { get; set; }

        [MaxLength(50)]
        public string PictureUrl { get; set; }

        [MaxLength(50)]
        public string TrailerUrl { get; set; }

        //Defines relationship between movies and franchises, a movie can only belong to one franchise
        public int FranchiseId { get; set; }
        public Franchise Franchise { get; set; }

        //Defines relationship between movies and characters,one movie can have many characters, hence the ICollection of characters, and one character can have many movies
        public ICollection<Character> Characters { get; set; }
    }
}