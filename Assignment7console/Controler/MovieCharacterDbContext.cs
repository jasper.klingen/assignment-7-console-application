﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment7console.Models
{
    public class MovieCharacterDbContext : DbContext
    {
        //Generates character table
        public DbSet<Character> Character { get; set; }

        //Generates Franchise table
        public DbSet<Franchise> Franchise { get; set; }

        //Generates Movie table
        public DbSet<Movie> Movie { get; set; }


        //Building the connection to the database
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer($"Data Source = {ConnectionStringHelper.ConnectionString()}; Initial Catalog = MovieCharacter; Integrated Security = true;");
        }
        /// <summary>
        /// Seeding the database
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Character>().HasData(new Character { CharacterId = 1, FullName = "Usain Bolt" });
            modelBuilder.Entity<Character>().HasData(new Character { CharacterId = 2, FullName = "Forrest Gump" });
            modelBuilder.Entity<Character>().HasData(new Character { CharacterId = 3, FullName = "Barack Obama" });

            modelBuilder.Entity<Franchise>().HasData(new Franchise { FranchiseId = 1,  Name = "House of Cards"});
            modelBuilder.Entity<Franchise>().HasData(new Franchise { FranchiseId = 2, Name = "Disney" });
            modelBuilder.Entity<Franchise>().HasData(new Franchise { FranchiseId = 3, Name = "Kung Fu Panda" });

            modelBuilder.Entity<Movie>().HasData(new Movie { MovieId = 1, MovieTitle = "Kung Fu Panda 2", Year = 2005, FranchiseId = 3 });
            modelBuilder.Entity<Movie>().HasData(new Movie { MovieId = 2, MovieTitle = "The Making of House of Cards", Year = 2012, FranchiseId = 1 });
            modelBuilder.Entity<Movie>().HasData(new Movie { MovieId = 3, MovieTitle = "Bambi", Year = 1942, FranchiseId = 2 });

            modelBuilder.Entity<Character>()
                .HasMany(c => c.Movies)
                .WithMany(m => m.Characters)
                .UsingEntity<Dictionary<string, object>>(
                    "MovieCharacter",
                    r => r.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                    l => l.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                    je =>
                    {
                        je.HasKey("MovieId", "CharacterId");
                        je.HasData(
                            new { MovieId = 1, CharacterId = 1 },
                            new { MovieId = 1, CharacterId = 2 },
                            new { MovieId = 2, CharacterId = 2 },
                            new { MovieId = 2, CharacterId = 3 },
                            new { MovieId = 3, CharacterId = 1 },
                            new { MovieId = 3, CharacterId = 3 }
                        );
                    });
        }
    }
}
