**Week 7 assignment**

For the .NET Full Stack Noroff course, we had to create a Web API using a Code First workflow and the ASP.NET Core Web API framework in Visual Studio.
For Appendix A we had to create a database using the Microsoft Entity framework. 
For Appendix B we had to create a Web API in ASP.NET Core using Swagger for documentation. 

Appendix A & B exist as seperate projects inside the Assignment7console solution.
The code for Appendix A can be found in the Assignment7console project.
The code for Appendix B can be found in the Assignment7API project.


**Appendix A:**

A C# project was written to create the following database structure:
- There are tables containing records for Movies, Characters and Franchises
- Each of these tables has at least 3 seeded records
- Each Movie is part of zero or one Franchise
- Each Movie has zero, one or mutiple Characters
- Each Character can play in zero, one or mutiple Movies
- Each Franchise exists of zero, one or mutiple Movies


**Appendix B:**
A C# project was written to create the following API endpoints:
- Full CRUD for Movies, Characters and Franchises (R contains "Read all" and "Read one" endpoints)
- Add or Remove an Existing Character from an Existing movie
- Add an existing Franchise to a Movie or Remove the chosen Franchise from a Movie
- Get all Movies in a Franchise
- Get all Characters in a Movie
- Get all Characters in a Franchise
