﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment7ASP.Models.DTOs.Movie
{
    public class MovieEditDTO
    {
        public int MovieId { get; set; }

        public string MovieTitle { get; set; }

        public string Genre { get; set; }

        public int Year { get; set; }

        public string Director { get; set; }

        public string PictureUrl { get; set; }

        public string TrailerUrl { get; set; }

        public int? FranchiseDTO { get; set; }
    }
}
