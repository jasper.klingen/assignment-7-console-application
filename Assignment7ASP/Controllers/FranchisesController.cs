﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment7ASP.Models;
using AutoMapper;
using Assignment7ASP.Models.DTOs.Franchise;
using Assignment7ASP.Models.Domain;
using System.Net.Mime;
using Assignment7ASP.Models.DTOs.Movie;
using Assignment7ASP.Models.DTOs.Character;

namespace Assignment7ASP.Controllers
{
    [Route("api/franchises")]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [ApiController]
    public class FranchisesController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;

        public FranchisesController(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all Franchises by FranchiseId
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchise()
        {
            return _mapper.Map<List<FranchiseReadDTO>>(await _context.Franchise.Include(f => f.Movies).ToListAsync());
        }

        /// <summary>
        /// Get one specific Franchise by their FranchiseId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            var franchiseChecker = _mapper.Map<FranchiseReadDTO>(await _context.Franchise.FindAsync(id));

            if (franchiseChecker == null)
            {
                return NotFound();
            }
            var franchise = _mapper.Map<FranchiseReadDTO>(await _context.Franchise.Include(f => f.Movies).Where(f => f.FranchiseId == id).FirstOrDefaultAsync());

            return franchise;
        }

        /// <summary>
        /// Update a specific Franchise by their FranchiseId
        /// </summary>
        /// <param name="id"></param>
        /// <param name="franchiseDto"></param>
        /// <returns></returns>
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> PutFranchise(int id, FranchiseEditDTO franchiseDto)
        {
            if (id != franchiseDto.FranchiseId)
            {
                return BadRequest();
            }
            Franchise domainFranchise = _mapper.Map<Franchise>(franchiseDto);
            _context.Entry(domainFranchise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return _mapper.Map<FranchiseReadDTO>(await _context.Franchise.Include(f => f.Movies).Where(f => f.FranchiseId == id).SingleAsync());
        }

        /// <summary>
        /// Add a new Franchise to the database
        /// </summary>
        /// <param name="franchiseDto"></param>
        /// <returns></returns>
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<FranchiseReadDTO>> PostFranchise(FranchiseCreateDTO franchiseDto)
        {
            Franchise domainFranchise = _mapper.Map<Franchise>(franchiseDto);

            domainFranchise = await AddFranchiseAsync(domainFranchise);
            return CreatedAtAction("GetFranchise",
                new { id = domainFranchise.FranchiseId },
                _mapper.Map<FranchiseReadDTO>(domainFranchise));
        }

        private async Task<Franchise> AddFranchiseAsync(Franchise domainFranchise)
        {
            _context.Franchise.Add(domainFranchise);
            await _context.SaveChangesAsync();
            return domainFranchise;
        }

        /// <summary>
        /// Delete a specific Franchise by their FranchiseId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            FranchiseEditDTO franchiseDto = new()
            {
                FranchiseId = id
            };
            if (id != franchiseDto.FranchiseId)
            {
                return BadRequest();
            }
            Franchise domainFranchise = _mapper.Map<Franchise>(franchiseDto);

            _context.Entry(domainFranchise).State = EntityState.Modified;

            domainFranchise = await RemoveFranchiseAsync(domainFranchise);
            return NoContent();
        }

        private async Task<Franchise> RemoveFranchiseAsync(Franchise domainFranchise)
        {
            _context.Franchise.Remove(domainFranchise);
            await _context.SaveChangesAsync();
            return domainFranchise;
        }

        /// <summary>
        /// Get all the Movies in a specific Franchise by FranchiseId
        /// </summary>
        /// <param name="franchiseID"></param>
        /// <returns></returns>
        [HttpGet("{franchiseID}/movies")]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMoviesInFranchise(int franchiseID)
        {
            var franchiseChecker = _mapper.Map<FranchiseReadDTO>(await _context.Franchise.FindAsync(franchiseID));

            if (franchiseChecker == null)
            {
                return NotFound();
            }
            return _mapper.Map<List<MovieReadDTO>>(await _context.Movie.Include(m => m.Characters).Where(m => m.FranchiseId == franchiseID).ToListAsync());
        }

        /// <summary>
        /// Get all the Characters in a specific Franchise by FranchiseId
        /// </summary>
        /// <param name="franchiseID"></param>
        /// <returns></returns>
        [HttpGet("{franchiseID}/characters")]
        public async Task<ActionResult<HashSet<CharacterReadDTO>>> GetCharactersInFranchise(int franchiseID)
        {
            var franchiseChecker = _mapper.Map<FranchiseReadDTO>(await _context.Franchise.FindAsync(franchiseID));
            if (franchiseChecker == null)
            {
                return NotFound();
            }
            var moviesInFranchise = _mapper.Map<List<MovieReadDTO>>(await _context.Movie.Include(m => m.Characters).Where(m => m.FranchiseId == franchiseID).ToListAsync());
            HashSet<CharacterReadDTO> characterList = new();
            foreach (MovieReadDTO Movie in moviesInFranchise)
            {
                var movie = _context.Movie.Include(m => m.Characters).ThenInclude(c => c.Movies).Where(m => m.MovieId == Movie.MovieId).Single();
                foreach (Character characterInFranchise in movie.Characters)
                {
                    CharacterReadDTO characterReadDTO = _mapper.Map<CharacterReadDTO>(characterInFranchise);
                    characterList.Add(characterReadDTO);
                }
            }
            return characterList;

        }

        private bool FranchiseExists(int id)
        {
            return _context.Franchise.Any(e => e.FranchiseId == id);
        }
    }
}
