﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment7ASP.Models;
using Assignment7ASP.Models.Domain;
using System.Net.Mime;
using AutoMapper;
using Assignment7ASP.Models.DTOs.Character;

namespace Assignment7ASP.Controllers
{
    [Route("api/characters")]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [ApiController]
    public class CharacterController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;

        public CharacterController(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all Characters by CharacterId
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacter()
        {
            return _mapper.Map<List<CharacterReadDTO>>(await _context.Character.Include(c => c.Movies).ToListAsync());
        }

        /// <summary>
        /// Get one specific Character by their CharacterId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id)
        {
            
            var characterChecker = _mapper.Map<CharacterReadDTO>(await _context.Character.Include(c => c.Movies).Where(c => c.CharacterId == id).FirstOrDefaultAsync());

            if (characterChecker == null)
            {
                return NotFound();
            }
            var character = _mapper.Map<CharacterReadDTO>(await _context.Character.Include(c => c.Movies).Where(c => c.CharacterId == id).FirstOrDefaultAsync());

            return character;
        }

        /// <summary>
        /// Update a specific Character by their CharacterId
        /// </summary>
        /// <param name="id"></param>
        /// <param name="characterDto"></param>
        /// <returns></returns>
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> PutCharacter(int id, CharacterEditDTO characterDto) //does not return edited values
        {
            if (id != characterDto.CharacterId)
            {
                return BadRequest();
            }
            Character domainCharacter = _mapper.Map<Character>(characterDto);
            _context.Entry(domainCharacter).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return _mapper.Map<CharacterReadDTO>(await _context.Character.Include(c => c.Movies).Where(c => c.CharacterId == id).SingleAsync());
        }

        /// <summary>
        /// Add a new Character to the database
        /// </summary>
        /// <param name="characterDto"></param>
        /// <returns></returns>
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<CharacterCreateDTO>> PostCharacter(CharacterCreateDTO characterDto)
        {
            Character domainCharacter = _mapper.Map<Character>(characterDto);
            domainCharacter = await AddCharacterAsync(domainCharacter);

            return CreatedAtAction("GetCharacter", 
                new { id = domainCharacter.CharacterId }, 
                _mapper.Map<CharacterReadDTO>(domainCharacter));
        }
        private async Task<Character> AddCharacterAsync(Character domainCharacter)
        {
            _context.Character.Add(domainCharacter);
            await _context.SaveChangesAsync();
            return domainCharacter;
        }

        /// <summary>
        /// Delete a specific Character by their CharacterId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            if (!CharacterExists(id))
            {
                return NotFound();
            }
            CharacterEditDTO characterDto = new()
            {
                CharacterId = id
            };
            if (id != characterDto.CharacterId)
            {
                return BadRequest();
            }
            Character domainCharacter = _mapper.Map<Character>(characterDto);

            _context.Entry(domainCharacter).State = EntityState.Modified;

            domainCharacter = await RemoveCharacterAsync(domainCharacter);
            //var character = await _context.Character.FindAsync(id);
            //if (character == null)
            //{
            //    return NotFound();
            //}

            //_context.Character.Remove(character);
            //await _context.SaveChangesAsync();

            return NoContent();
        }

        private async Task<Character> RemoveCharacterAsync(Character domainCharacter)
        {
            _context.Character.Remove(domainCharacter);
            await _context.SaveChangesAsync();
            return domainCharacter;
        }

        private bool CharacterExists(int id)
        {
            return _context.Character.Any(e => e.CharacterId == id);
        }
    }
}
