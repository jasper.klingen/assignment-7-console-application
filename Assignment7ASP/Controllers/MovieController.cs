﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment7ASP.Models;
using AutoMapper;
using Assignment7ASP.Models.DTOs.Movie;
using Assignment7ASP.Models.Domain;
using System.Net.Mime;
using Assignment7ASP.Models.DTOs.Character;

namespace Assignment7ASP.Controllers
{
    [Route("api/movie")]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [ApiController]
    public class MovieController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;

        public MovieController(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all Movies by MovieId
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovie()
        {
            return _mapper.Map<List<MovieReadDTO>>(await _context.Movie.Include(m=>m.Franchise).Include(m=>m.Characters).ToListAsync());
        }

        /// <summary>
        /// Get one specific Movie by their MovieId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {
            var movieChecker = _mapper.Map<MovieReadDTO>(await _context.Movie.FindAsync(id));

            if (movieChecker == null)
            {
                return NotFound();
            }
            var movie = _mapper.Map<MovieReadDTO>(await _context.Movie.Include(m => m.Franchise).Include(m => m.Characters).Where(m => m.MovieId == id).FirstOrDefaultAsync());
            return movie;
        }

        /// <summary>
        /// Update a specific Movie by their MovieId
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movieDto"></param>
        /// <returns></returns>
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<ActionResult<MovieReadDTO>> PutMovie(int id, MovieEditDTO movieDto)//does not return edited values
        {
            if (id != movieDto.MovieId)
            {
                return BadRequest();
            }

            Movie domainMovie = _mapper.Map<Movie>(movieDto); 
            _context.Entry(domainMovie).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return _mapper.Map<MovieReadDTO>(await _context.Movie.Include(m => m.Franchise).Include(m => m.Characters).Where(m => m.MovieId == id).SingleAsync());
        }

        /// <summary>
        /// Add a new Movie to the database
        /// </summary>
        /// <param name="movieDto"></param>
        /// <returns></returns>
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<MovieReadDTO>> PostMovie(MovieCreateDTO movieDto)
        {
            Movie domainMovie = _mapper.Map<Movie>(movieDto);
            
            
            domainMovie = await AddMovieAsync(domainMovie);
            return CreatedAtAction("GetMovie",
                new { id = domainMovie.MovieId },
                _mapper.Map<MovieReadDTO>(domainMovie));
        }

        private async Task<Movie> AddMovieAsync(Movie domainMovie)
        {
            _context.Movie.Add(domainMovie);
            await _context.SaveChangesAsync();
            return domainMovie;
        }

        /// <summary>
        /// Delete a specific Movie by their MovieId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            MovieEditDTO movieDto = new()
            {
                MovieId = id
            };
            if (id != movieDto.MovieId)
            {
                return BadRequest();
            }
            Movie domainMovie = _mapper.Map<Movie>(movieDto);

            _context.Entry(domainMovie).State = EntityState.Modified;

            domainMovie = await RemoveMovieAsync(domainMovie);


            return NoContent();
        }
        private async Task<Movie> RemoveMovieAsync(Movie domainMovie)
        {
            _context.Movie.Remove(domainMovie);
            await _context.SaveChangesAsync();
            return domainMovie;
        }

        /// <summary>
        /// Add a Character by their CharacterId to an Existing Movie by their MovieId
        /// </summary>
        /// <param name="movieID"></param>
        /// <param name="characterId"></param>
        /// <returns></returns>
        [HttpPost("{movieID}/characters/{characterId}")]
        public async Task<ActionResult<CharacterReadDTO>> PostCharacterInMovie(int movieID, int characterId)
        {
            var movieChecker = _mapper.Map<MovieReadDTO>(await _context.Movie.FindAsync(movieID));

            if (movieChecker == null)
            {
                return NotFound();
            }
            Movie movie = _mapper.Map<Movie>(await _context.Movie.Include(m => m.Franchise).Include(m => m.Characters).Where(m => m.MovieId == movieID).FirstOrDefaultAsync());

            var characterChecker = _mapper.Map<CharacterReadDTO>(await _context.Character.FindAsync(characterId));

            if (characterChecker == null)
            {
                return NotFound();
            }
            Character character = _mapper.Map<Character>(await _context.Character.Include(c => c.Movies).Where(c => c.CharacterId == characterId).FirstOrDefaultAsync());


            movie.Characters.Add(character);
            
            await _context.SaveChangesAsync();
            MovieReadDTO movieDto = _mapper.Map<MovieReadDTO>(await _context.Movie.Include(m => m.Franchise).Include(m => m.Characters).Where(m => m.MovieId == movieID).FirstOrDefaultAsync());
            return CreatedAtAction("GetMovie", new { id = movieDto.MovieId }, movieDto);
        }

        /// <summary>
        /// Delete a Character by their CharacterId in an Existing Movie by their MovieId
        /// </summary>
        /// <param name="movieID"></param>
        /// <param name="characterId"></param>
        /// <returns></returns>
        [HttpDelete("{movieID}/characters/{characterId}")]
        public async Task<ActionResult<Movie>> DeleteCharacterInMovie(int movieID, int characterId)
        {
            var movieChecker = _mapper.Map<MovieReadDTO>(await _context.Movie.FindAsync(movieID));

            if (movieChecker == null)
            {
                return NotFound();
            }
            Movie movie = _mapper.Map<Movie>(await _context.Movie.Include(m => m.Franchise).Include(m => m.Characters).Where(m => m.MovieId == movieID).FirstOrDefaultAsync());

            var characterChecker = _mapper.Map<CharacterReadDTO>(await _context.Character.FindAsync(characterId));

            if (characterChecker == null)
            {
                return NotFound();
            }
            Character character = _mapper.Map<Character>(await _context.Character.Include(c => c.Movies).Where(c => c.CharacterId == characterId).FirstOrDefaultAsync());

            movie.Characters.Remove(character);

            await _context.SaveChangesAsync();
            MovieReadDTO movieDto = _mapper.Map<MovieReadDTO>(await _context.Movie.Include(m => m.Franchise).Include(m => m.Characters).Where(m => m.MovieId == movieID).FirstOrDefaultAsync());
            return CreatedAtAction("GetMovie", new { id = movieDto.MovieId }, movieDto);

        }

        /// <summary>
        /// Add a Franchise by their FranchiseId to an existing Movie by their MovieId
        /// </summary>
        /// <param name="movieID"></param>
        /// <param name="franchiseId"></param>
        /// <returns></returns>
        [HttpPost("{movieID}/franchise/{franchiseId}")]
        public async Task<ActionResult<MovieReadDTO>> PostFranchiseInMovie(int movieID, int franchiseId)
        {
            Movie movie = _context.Movie.Include(m => m.Franchise).Where(m => m.MovieId == movieID).Single();

            if (movie == null)
            {
                return NotFound();
            }

            Franchise franchise = _context.Franchise.Find(franchiseId);

            if (franchise == null)
            {
                return NotFound();
            }

            movie.Franchise = franchise;

            await _context.SaveChangesAsync();
            MovieReadDTO movieDto = _mapper.Map<MovieReadDTO>(await _context.Movie.Include(m => m.Franchise).Include(m => m.Characters).Where(m => m.MovieId == movieID).SingleAsync());
            return CreatedAtAction("GetMovie", new { id = movieDto.MovieId }, movieDto);

        }

        /// <summary>
        /// Remove the assigned Franchise from a specific Movie by MovieId
        /// </summary>
        /// <param name="movieID"></param>
        /// <returns></returns>
        [HttpDelete("{movieID}/franchise")]
        public async Task<ActionResult<MovieReadDTO>> DeleteFranchiseInMovie(int movieID)
        {
            Movie movie = _context.Movie.Include(m => m.Franchise).Where(m => m.MovieId == movieID).Single();

            if (movie == null)
            {
                return NotFound();
            }

            movie.Franchise = null;

            await _context.SaveChangesAsync();
            MovieReadDTO movieDto = _mapper.Map<MovieReadDTO>(await _context.Movie.Include(m => m.Franchise).Include(m => m.Characters).Where(m => m.MovieId == movieID).SingleAsync());

            return CreatedAtAction("GetMovie", new { id = movieDto.MovieId }, movieDto);

        }

        /// <summary>
        /// Get all the Characters in a specific Movie by MovieId
        /// </summary>
        /// <param name="movieID"></param>
        /// <returns></returns>
        [HttpGet("{movieID}/characters")]
        public async Task<ActionResult<List<CharacterReadDTO>>> GetCharactersInMovie(int movieID)
        {

            var movieChecker = _mapper.Map<MovieReadDTO>(await _context.Movie.FindAsync(movieID));

            if (movieChecker == null)
            {
                return NotFound();
            }

            return _mapper.Map<List<CharacterReadDTO>>
                (await _context.Movie
                .Where(m => m.MovieId == movieID)
                .Include(m => m.Characters)
                .ThenInclude(c => c.Movies)
                .Select(m => m.Characters).SingleAsync());
        }

        private bool MovieExists(int id)
        {
            return _context.Movie.Any(e => e.MovieId == id);
        }
    }
}
