﻿using Assignment7ASP.Models.DTOs.Movie;
using Assignment7ASP.Models.Domain;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment7ASP.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieReadDTO>()
            .ForMember(mdto => mdto.Franchise, opt => opt
            .MapFrom(m => m.FranchiseId))
            .ForMember(mdto => mdto.Characters, opt => opt
            .MapFrom(m => m.Characters.Select(c => c.CharacterId).ToList()))
            .ReverseMap();

            CreateMap<Movie, MovieCreateDTO>()
            .ForMember(mdto => mdto.FranchiseDTO, opt => opt
            .MapFrom(m => m.FranchiseId))
            .ReverseMap();

            CreateMap<Movie, MovieEditDTO>()
            .ForMember(mdto => mdto.FranchiseDTO, opt => opt
            .MapFrom(m => m.FranchiseId))
            .ReverseMap();
        }
    }
}
