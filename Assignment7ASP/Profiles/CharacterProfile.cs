﻿using Assignment7ASP.Models.Domain;
using Assignment7ASP.Models.DTOs.Character;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment7ASP.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterReadDTO>()
            .ForMember(cdto => cdto.Movies, opt => opt
            .MapFrom(c => c.Movies.Select(m => m.MovieId).ToList()))
            .ReverseMap();

            CreateMap<Character, CharacterCreateDTO>()
            .ReverseMap();

            CreateMap<Character, CharacterEditDTO>()
            .ReverseMap();
        }

    }
}
