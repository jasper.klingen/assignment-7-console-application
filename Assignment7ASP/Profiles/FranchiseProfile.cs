﻿using Assignment7ASP.Models.Domain;
using Assignment7ASP.Models.DTOs.Franchise;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment7ASP.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseReadDTO>()
                .ForMember(fdto => fdto.Movies, opt => opt
                .MapFrom(f => f.Movies.Select(m => m.MovieId).ToList()))
                .ReverseMap();

            CreateMap<Franchise, FranchiseCreateDTO>()
                .ReverseMap();

            CreateMap<Franchise, FranchiseEditDTO>()
                .ReverseMap();
        }

    }
}
